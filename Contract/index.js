function Contract ({ abi, address, connex }) {
  if (!abi) {
    throw new Error('"abi" is required')
  }

  if (!address) {
    throw new Error('"address" is required')
  }

  if (!connex) {
    throw new Error('"connex" is required')
  }

  const account = connex.thor.account(address)
  this.account = account

  abi.forEach(singleAbi => {
    const { name, type, stateMutability } = singleAbi

    if (type === 'function' && stateMutability !== 'view') {
      this[name] = (...args) => account.method(singleAbi).asClause(...args)
    }

    if (type === 'function' && stateMutability === 'view') {
      this[name] = async (...args) => {
        const result = await account.method(singleAbi).call(...args)
        if (!result?.decoded) {
          return
        }

        if (!result.decoded['1']) {
          return result.decoded['0']
        }

        return result.decoded
      }
    }
  })
}

module.exports = Contract
