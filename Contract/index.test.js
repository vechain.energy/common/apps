const Contract = require('./index')

describe('Contract', () => {
  it.each`
  arg
  ${'abi'}
  ${'address'}
  ${'connex'}
  `('throws an error if "$arg" is missing as argument', ({ arg }) => {
    const args = getSampleArgs()
    delete (args[arg])

    expect(() => new Contract(args)).toThrowError(`"${arg}" is required`)
  })

  it('can be correctly instantiated', () => {
    const args = getSampleArgs()
    const contract = new Contract(args)

    expect(contract).toBeInstanceOf(Contract)
  })

  describe('account', () => {
    it('creates an AccountVisitor for the given address', () => {
      const address = '0x'
      const args = getSampleArgs()

      new Contract(args)
      expect(args.connex.thor.account).toHaveBeenCalledWith(address)
    })

    it('sets the AccountVisitor as attribute in "account"', () => {
      const visitor = { rand: Math.random() }
      const args = getSampleArgs()
      args.connex.thor.account.mockReturnValueOnce(visitor)

      const contract = new Contract(args)
      expect(contract.account).toEqual(visitor)
    })
  })

  describe('view functions', () => {
    it('maps all abi functions with stateMutability=view as contract method()', () => {
      const abi = getSampleAbi([{
        name: 'testFunc',
        stateMutability: 'view',
        type: 'function'
      }])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)
      abi.filter(({ type, stateMutability }) => type === 'function' && stateMutability === 'view')
        .forEach(abi => {
          expect(contract[abi.name]).toBeInstanceOf(Function)
        })
    })

    it('calls method(abi).call() for function views', async () => {
      const testAbi = { name: 'testFunc', stateMutability: 'view', type: 'function' }
      const mockCall = jest.fn()
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)
      contract.account.method.mockReturnValueOnce({ call: mockCall })
      await contract.testFunc()
      expect(contract.account.method).toHaveBeenCalledWith(testAbi)
      expect(mockCall).toHaveBeenCalledTimes(1)
    })

    it('passes all args to method(abi).call()', async () => {
      const testArgs = [Math.random(), Math.random(), Math.random()]
      const testAbi = { name: 'testFunc', stateMutability: 'view', type: 'function' }
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)

      const mockCall = jest.fn()
      contract.account.method.mockReturnValue({ call: mockCall })

      await contract.testFunc(...testArgs)
      expect(mockCall).toHaveBeenCalledWith(...testArgs)
    })

    it('returns the decoded part', async () => {
      const testArgs = [Math.random(), Math.random(), Math.random()]
      const testAbi = { name: 'testFunc', stateMutability: 'view', type: 'function' }
      const decoded = { 0: Math.random(), 1: Math.random() }
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)

      const mockCall = jest.fn(() => ({ decoded }))
      contract.account.method.mockReturnValue(mockCall)

      const result = await contract.testFunc(...testArgs)
      expect(result).toEqual(decoded)
    })

    it('returns decoded[0] if only one value is in the results', async () => {
      const testArgs = [Math.random(), Math.random(), Math.random()]
      const testAbi = { name: 'testFunc', stateMutability: 'view', type: 'function' }
      const decoded = { 0: Math.random() }
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)

      const mockCall = jest.fn(() => ({ decoded }))
      contract.account.method.mockReturnValue(mockCall)

      const result = await contract.testFunc(...testArgs)
      expect(result).toEqual(decoded['0'])
    })
  })

  describe('modifier functions', () => {
    it('maps all abi functions with stateMutability != view as contract method()', () => {
      const abi = getSampleAbi([{
        name: 'testFunc',
        stateMutability: 'nonpayable',
        type: 'function'
      }])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)
      abi.filter(({ type, stateMutability }) => type === 'function' && stateMutability !== 'view')
        .forEach(abi => {
          expect(contract[abi.name]).toBeInstanceOf(Function)
        })
    })

    it('calls method(abi).asClause() for function', async () => {
      const testAbi = { name: 'testFunc', stateMutability: 'nonpayable', type: 'function' }
      const mockAsClause = jest.fn()
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)
      contract.account.method.mockReturnValue({ asClause: mockAsClause })
      await contract.testFunc()
      expect(contract.account.method).toHaveBeenCalledWith(testAbi)
      expect(mockAsClause).toHaveBeenCalledTimes(1)
    })

    it('passes all args to method(abi).call()', async () => {
      const testArgs = [Math.random(), Math.random(), Math.random()]
      const testAbi = { name: 'testFunc', stateMutability: 'nonpayable', type: 'function' }
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)

      const mockAsClause = jest.fn()
      contract.account.method.mockReturnValue({ asClause: mockAsClause })

      await contract.testFunc(...testArgs)
      expect(mockAsClause).toHaveBeenCalledWith(...testArgs)
    })

    it('returns the raw result', async () => {
      const testArgs = [Math.random(), Math.random(), Math.random()]
      const testAbi = { name: 'testFunc', stateMutability: 'nonpayable', type: 'function' }
      const clause = { rand: Math.random() }
      const abi = getSampleAbi([testAbi])

      const args = getSampleArgs({ abi })

      const contract = new Contract(args)

      const mockAsClause = jest.fn(() => clause)
      contract.account.method.mockReturnValue({ asClause: mockAsClause })

      const result = await contract.testFunc(...testArgs)
      expect(result).toEqual(clause)
    })
  })
})

function getSampleArgs (defaults = {}) {
  return {
    abi: [],
    address: '0x',
    connex: {
      thor: {
        account: jest.fn(() => ({
          method: jest.fn(() => ({
            call: jest.fn()
          }))
        }))
      }
    },
    ...defaults
  }
}

function getSampleAbi (defaults = []) {
  return [
    {
      constant: true,
      inputs: [],
      name: 'totalSupply',
      outputs: [
        {
          name: '',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    ...defaults
  ]
}
