A wrapper for connex that combines it with known ABIs and contracts to improve development setups.

_This is a work in progress trialing different use cases_

# Installation

```shell
yarn add @vechain.energy/apps @vechain.energy/apps/registry
```

* `@vechain.energy/apps` is the wrapper and simplifier for blockchain interaction
* `@vechain.energy/apps/registry` is a list of known contracts and ABIs to be used by alias

# Usage

```js
import Apps from '@vechain.energy/apps';
import '@vechain.energy/apps/registry';

const vtho = await Apps.getContract('VTHO')

// read operations return the decoded results
const totalSupply = await vtho.totalSupply()
const balance = await vtho.balanceOf('0x04Ad3f13050cc766169433062BcDbB367B616986')

// write operations return the clauses
const transferClause = await vtho.transfer('0x04Ad3f13050cc766169433062BcDbB367B616986', '0x04Ad3f13050cc766169433062BcDbB367B616986', 1)

// standards for a known address
const noNerdsTablets = Apps.getContractAt('ERC721', '0x77fe6041fa5beb0172c9ab6014b4d8d5099f0a23')
const totalSupply = await noNerdsTablets.totalSupply()
expect(totalSupply).toBeTruthy()

// multiple standards for a known address
const noNerdsTablets = await Apps.getContractAt(['ERC20', 'ERC721'], '0x77fe6041fa5beb0172c9ab6014b4d8d5099f0a23')
const totalSupply = await noNerdsTablets.totalSupply()
expect(totalSupply).toBeTruthy()

```

## Modifying functions return clauses

```js
const Apps = require('@vechain.energy/apps')
await Apps.setConnex('https://testnet.veblocks.net')

const abiCounter = [{ "inputs": [], "name": "counter", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "increment", "outputs": [], "stateMutability": "nonpayable", "type": "function" }]
const contractAddress = '0x8384738c995d49c5b692560ae688fc8b51af1059'

const counter = await Apps.getContractAt(abiCounter, contractAddress)
const clause = await counter.increment()

const connex = Apps.getConnex()
const { txid } = connex.vendor
  .sign("tx", [clause])
  .delegate("https://sponsor-testnet.vechain.energy/by/90")
  .comment("increment counter by +1")
  .request();
```

## Registry

Access address and abi information directly.

```js
import Apps from '@vechain.energy/apps';

const { address, abi } = Apps.Registry('VTHO')

console.log(address)
/**
{
  "test": "0x0000000000000000000000000000456E65726779",
  "main": "0x0000000000000000000000000000456E65726779"
}
**/

console.log(abi)
/**
[
  {
    "constant": true,
    "inputs": [],
    "name": "name",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "pure",
    "type": "function"
  },
  …
**/
```

Register custom aliases:

```js
import Apps from '@vechain.energy/apps';

const abi = []
const address = {
  "test": "0x0000000000000000000000000000456E65726779",
  "main": "0x0000000000000000000000000000456E65726779"
}
Apps.Registry.register({
  name: 'VTHO',
  abi,
  address
})

```


## Contract

Access contracts with all required information directly :

```js
import Apps from '@vechain.energy/apps';

const connex = new Connex({
  node: "https://mainnet.veblocks.net",
  network: "main"
});

const { address, abi } = Apps.Registry('VTHO')
const contract = Apps.Contract({ address, abi, connex })
const totalSupply = await contract.totalSupply()

```

## Custom Connex

```js
import Apps from '@vechain.energy/apps';

const connex = new Connex({
  node: "https://mainnet.veblocks.net",
  network: "main"
});

// set custom connex instance or options
// if not used, will create one automatically connecting to the MainNet
Apps.setConnex(connex)
```