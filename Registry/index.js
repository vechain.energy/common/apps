const REGISTRY_CACHE = {}

function loadData(nameOrNames) {
  if (typeof (nameOrNames) === 'string') {
    return lookupByName(nameOrNames)
  }

  return nameOrNames.reduce((result, name) => {
    const { address, abi } = lookupByName(name)
    result.address = address
    result.abi.push(...abi)
    return result
  }, { abi: [] })
}

function lookupByName(name) {
  if ( REGISTRY_CACHE[name] ) {
    return REGISTRY_CACHE[name]
  }

  try {
    const folderName = String(name).toLowerCase().replace(/[^a-z0-9]/g, '-')
    const address = require(`./${folderName}/address.json`)
    const abi = require(`./${folderName}/abi.json`)

    REGISTRY_CACHE[name] = { address, abi }
    return REGISTRY_CACHE[name]
  } catch (err) {
    throw new Error(`${name} is not known`)
  }
}

loadData.register = function register({ name, abi, address }) {
  REGISTRY_CACHE[name] = { abi, address }
}

module.exports = loadData
