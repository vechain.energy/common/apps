const Registry = require('./index')

describe('Registry', () => {
  it('throws an error if a name can not be resolved', () => {
    expect(() => Registry('testFailure')).toThrowError('testFailure is not known')
  })

  it('returns an object for a known application', () => {
    const app = Registry('vtho')
    expect(app).toBeInstanceOf(Object)
  })

  it('returns address with address.json from the app', () => {
    const { address } = Registry('vtho')
    expect(address).toEqual(require('./vtho/address.json'))
  })

  it('returns address with abi.json from the app', () => {
    const { abi } = Registry('vtho')
    expect(abi).toEqual(require('./vtho/abi.json'))
  })

  it('combines multiple abi lists if name is given as list', () => {
    const { abi } = Registry(['erc721', 'erc20'])
    expect(abi).toEqual([...require('./erc721/abi.json'), ...require('./erc20/abi.json')])
  })

  describe('register(alias, abi, address)', () => {
    it('supports register(alias, abi, address)', () => {
      const address = require('./vtho/address.json')
      const abi = require('./vtho/abi.json')
      Registry.register({ name: 'TEST', abi, address })

      const entry = Registry('TEST')
      expect(entry.address).toEqual(address)
      expect(entry.abi).toEqual(abi)
    })

    it('overwrites previous registrations', () => {
      const address = require('./vtho/address.json')
      const abi = require('./vtho/abi.json')
      Registry.register({ name: 'TEST', address: require('./erc20/address.json'), abi: require('./erc20/abi.json') })
      Registry.register({ name: 'TEST', abi, address })

      const entry = Registry('TEST')
      expect(entry.address).toEqual(address)
      expect(entry.abi).toEqual(abi)
    })

    it('overwrites entries from file', () => {
      const address = require('./vtho/address.json')
      const abi = require('./vtho/abi.json')
      Registry.register({ name: 'ERC20', abi, address })

      const entry = Registry('ERC20')
      expect(entry.address).toEqual(address)
      expect(entry.abi).toEqual(abi)
    })
  })
})
