const Apps = require('./')

async function main () {
  await Apps.setConnex('https://testnet.veblocks.net')
  const vtho = await Apps.getContract('VTHO')

  // read operations return the decoded results
  const totalSupply = await vtho.totalSupply()
  console.log(`Total VTHO Supply: ${totalSupply}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
