const { Framework } = require('@vechain/connex-framework')
const { Driver, SimpleNet } = require('@vechain/connex-driver')

async function getConnexInstance (url) {
  const driver = await Driver.connect(
    new SimpleNet(url)
  )

  return new Framework(driver)
}

module.exports = getConnexInstance
