const Registry = require('./Registry')
const Contract = require('./Contract')
const getConnexInstance = require('./getConnexInstance')

let defaultConnex
const DEFAULT_NETWORK_URL = 'https://mainnet.veblocks.net'
const GENESIS_TO_CHAIN = {
  '0x00000000851caf3cfdb6e899cf5958bfb1ac3413d346d43539627e6be7ec1b4a': 'main',
  '0x000000000b2bce3c70bc649a02749e8687721b09ed2e15997f466536b20bb127': 'test'
}

async function getContract(contractName, _connex = defaultConnex) {
  if (!_connex && !defaultConnex) {
    await setConnex(DEFAULT_NETWORK_URL)
  }
  const connex = _connex || defaultConnex

  const { address, abi } = Registry(contractName)
  const chain = GENESIS_TO_CHAIN[defaultConnex?.thor?.genesis?.id] || 'main'

  return new Contract({ address: typeof (address) === 'string' ? address : address?.[chain], abi, connex })
}

async function getContractAt(contractNameOrAbi, address, _connex) {
  if (!_connex && !defaultConnex) {
    await setConnex(DEFAULT_NETWORK_URL)
  }
  const connex = _connex || defaultConnex

  if (Array.isArray(contractNameOrAbi) && contractNameOrAbi[0].type) {
    return new Contract({ address, abi: contractNameOrAbi, connex })
  }

  const { abi } = Registry(contractNameOrAbi)
  return new Contract({ address, abi, connex })
}

async function setConnex(connexOrUrl) {
  if (typeof (connexOrUrl) === 'string') {
    defaultConnex = await getConnexInstance(connexOrUrl)
  } else {
    defaultConnex = connexOrUrl
  }
}
const getConnex = () => defaultConnex

module.exports = { getContract, getContractAt, Registry, setConnex, getConnex }
