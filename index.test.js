const Apps = require('./index')

const mockRegistry = jest.fn()
const Registry = require('./Registry')
jest.mock('./Registry', () => jest.fn(() => mockRegistry))

const mockContract = jest.fn()
const Contract = require('./Contract')
jest.mock('./Contract', () => jest.fn(() => mockContract))

const mockGetConnexInstance = { thor: { genesis: { id: '' } } }
const getConnexInstance = require('./getConnexInstance')
jest.mock('./getConnexInstance', () => jest.fn(() => mockGetConnexInstance))

describe('Apps', () => {
  it('exports Apps.Registry', () => {
    expect(Apps.Registry).toEqual(require('./Registry'))
  })

  describe('setConnex(connex)', () => {
    it('sets a new connex instance', () => {
      const testConnex = { rand: Math.random() }
      Apps.setConnex(testConnex)
      const connex = Apps.getConnex()
      expect(connex).toEqual(testConnex)
    })
  })
  describe('setConnex(url)', () => {
    it('creates a new connex instance for the given url', async () => {
      const url = 'https://testnet.some.url'
      await Apps.setConnex(url)
      expect(getConnexInstance).toHaveBeenCalledWith(url)
    })

    it('remembers the new connex instance for the given url', async () => {
      const testConnex = { rand: Math.random() }
      const url = 'https://testnet.some.url'

      getConnexInstance.mockReturnValue(testConnex)
      await Apps.setConnex(url)
      const connex = Apps.getConnex(url)
      expect(connex).toEqual(testConnex)
    })
  })

  describe('getConnex()', () => {
    it('returns the current connex instance', () => {
      const testConnex = { rand: Math.random() }
      Apps.setConnex(testConnex)
      const connex = Apps.getConnex()
      expect(connex).toEqual(testConnex)
    })
  })

  describe('getContract(contractName)', () => {
    it('is a function', () => {
      expect(Apps.getContract).toBeInstanceOf(Function)
    })

    it('fetches contract details from the Registry', () => {
      const name = `Random_${Math.random()}`
      Apps.getContract(name)
      expect(Registry).toHaveBeenCalledWith(name)
    })

    it('creates a Contract instance with registry data', () => {
      const { abi, address } = Registry('vtho')
      const connex = { rand: Math.random() }
      Apps.setConnex(connex)
      Apps.getContract('vtho')
      expect(Contract).toHaveBeenCalledWith({ abi, address, connex })
    })
  })

  describe('getContract(contractName, connex)', () => {
    it('supports an optional connex instance as second argument', () => {
      const connex = { rand: Math.random() }
      const { abi, address } = Registry('vtho')
      Apps.getContract('vtho', connex)
      expect(Contract).toHaveBeenCalledWith({ abi, address, connex })
    })
  })

  describe('getContractAt(contractName, address)', () => {
    it('is a function', () => {
      expect(Apps.getContractAt).toBeInstanceOf(Function)
    })

    it('fetches contract details from the Registry', () => {
      const name = `Random_${Math.random()}`
      Apps.getContractAt(name)
      expect(Registry).toHaveBeenCalledWith(name)
    })

    it('creates a Contract instance with registry data and given address', () => {
      const { abi } = Registry('erc721')
      const connex = { rand: Math.random() }
      const address = `0x${Math.random()}`
      Apps.setConnex(connex)

      Apps.getContractAt('erc721', address)
      expect(Contract).toHaveBeenCalledWith({ abi, address, connex })
    })
  })

  describe('getContractAt(contractName, address, connex)', () => {
    it('supports an optional connex instance as third argument', () => {
      const connex = { rand: Math.random() }
      const address = `0x${Math.random()}`
      const { abi } = Registry('erc721')
      Apps.getContractAt('erc721', address, connex)
      expect(Contract).toHaveBeenCalledWith({ abi, address, connex })
    })
  })

  describe('getContractAt(abi, address)', () => {
    it('supports an abi instead of contractName', () => {
      const abi = [{ inputs: [], name: 'counter', outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }], stateMutability: 'view', type: 'function' }, { inputs: [], name: 'increment', outputs: [], stateMutability: 'nonpayable', type: 'function' }]
      const connex = { rand: Math.random() }
      const address = `0x${Math.random()}`
      Apps.getContractAt(abi, address, connex)
      expect(Contract).toHaveBeenCalledWith({ abi, address, connex })
    })
  })
})
