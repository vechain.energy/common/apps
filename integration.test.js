describe('Integration', () => {
  it('works with reading data using example VTHO.totalSupply()', async () => {
    const Apps = require('./')

    const vtho = await Apps.getContract('VTHO')
    const totalSupply = await vtho.totalSupply()

    expect(totalSupply[0]).toBeTruthy()
  })

  it('works with reading data using example VTHO.balanceOf(address)', async () => {
    const Apps = require('./')
    await Apps.setConnex('https://testnet.veblocks.net')

    const vtho = await Apps.getContract('VTHO')
    const balance = await vtho.balanceOf('0x4f6FC409e152D33843Cf4982d414C1Dd0879277e')

    expect(balance).toBeTruthy()
  })

  it('works with known abi and custom address, example noNerdsTablets.totalSupply()', async () => {
    const Apps = require('./')
    await Apps.setConnex('https://mainnet.veblocks.net')

    const noNerdsTablets = await Apps.getContractAt(['ERC721'], '0x77fe6041fa5beb0172c9ab6014b4d8d5099f0a23')
    const totalSupply = await noNerdsTablets.totalSupply()

    expect(totalSupply).toBeTruthy()
  })

  it('works with modifying functions, example Counter.increment()', async () => {
    const Apps = require('./')
    await Apps.setConnex('https://testnet.veblocks.net')

    const abiCounter = [{ inputs: [], name: 'counter', outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }], stateMutability: 'view', type: 'function' }, { inputs: [], name: 'increment', outputs: [], stateMutability: 'nonpayable', type: 'function' }]
    const contractAddress = '0x8384738c995d49c5b692560ae688fc8b51af1059'

    const counter = await Apps.getContractAt(abiCounter, contractAddress)
    const clause = await counter.increment()
    expect(clause).toEqual({
      to: '0x8384738c995d49c5b692560ae688fc8b51af1059',
      value: '0',
      data: '0xd09de08a'
    })
  })
})
